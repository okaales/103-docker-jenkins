package com.mastertech.marketplace.controllers;

import java.net.URI;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mastertech.marketplace.helpers.URIBuilder;
import com.mastertech.marketplace.models.InvalidOffer;
import com.mastertech.marketplace.models.Offer;
import com.mastertech.marketplace.models.OfferFactory;
import com.mastertech.marketplace.models.Product;
import com.mastertech.marketplace.repositories.OfferRepository;
import com.mastertech.marketplace.repositories.ProductRepository;

@RestController
@RequestMapping("/offers")
public class OfferController {
	@Autowired
	OfferRepository offerRepository;

	@Autowired
	ProductRepository productRepository;

	@RequestMapping(method = RequestMethod.GET)
	public Iterable<?> getAll() {
		return offerRepository.findAll();
	}

	@RequestMapping(path="/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> get(@PathVariable int id) {
		Optional<Offer> offerQuery = offerRepository.findById(id);
		
		if(offerQuery.isPresent()) {
			Offer offer = offerQuery.get();
			return ResponseEntity.ok(offer);
		}
		
		return ResponseEntity.notFound().build();
	}
	
	@RequestMapping(path="/products/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> getAllByProduct(@PathVariable int id) {
		Optional<Product> productQuery = productRepository.findById(id);
		
		if(productQuery.isPresent()) {
			Product product = productQuery.get();
			
			Iterable<Offer> offers = offerRepository.getAllByProduct(product);
			
			return ResponseEntity.ok(offers);
		}
		
		return ResponseEntity.notFound().build();
	}

	@RequestMapping(method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody Offer offer) {

		OfferFactory factory = new OfferFactory();
		
		Offer completeOffer;
		try {
			completeOffer = factory.completeOffer(offer);
		} catch (InvalidOffer e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
		}
		
		Offer savedOffer = offerRepository.save(completeOffer);

		URI uri = URIBuilder.fromString("/user/" + savedOffer.getId());

		return ResponseEntity.created(uri).build();
	}

	@RequestMapping(path = "/approve/{id}", method = RequestMethod.POST)
	public ResponseEntity<?> approve(@PathVariable int id) {
		Optional<Offer> offerQuery = offerRepository.findById(id);

		if (offerQuery.isPresent()) {
			Offer offer = offerQuery.get();

			offer.setApproved(true);

			offerRepository.save(offer);

			return ResponseEntity.ok().build();
		}

		return ResponseEntity.notFound().build();
	}
}
